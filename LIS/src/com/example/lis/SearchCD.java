package com.example.lis;

// Class to display a list of CDs and search for the same.
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;

public class SearchCD extends ListActivity{
	
	protected EditText txtSearchCD;
	protected Button btnSearchCD ;
     static final String KEY_ITEM = "CD"; // parent node
     static final String KEY_CD_NAME = "Cd_Name"; // child node
     static final String KEY_CD_CONTENT = "Cd_Content";// child node
     
     ListAdapter adapter;
     private ArrayList<String> array_sort= new ArrayList<String>();
    
     ArrayList<HashMap<String, String>> menuItems;
     ArrayList<HashMap<String, String>> searchResults;
     
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_cds);
        
        
        btnSearchCD = (Button) findViewById(R.id.btnSearchCD);
        btnSearchCD.setEnabled(true);
        final String url = "http://10.100.55.81/Service1.asmx/ListAllCD"; // url of the web service
        menuItems = new ArrayList<HashMap<String, String>>();
     
        if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
        
        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(url); // getting the xml data from the method getXmlFromUrl.
        Document doc = parser.getDomElement(xml);// getting the DOM element.
        

        NodeList nl = doc.getElementsByTagName(KEY_ITEM); // getting the elements by the tag name
        
        for (int i = 0; i < nl.getLength(); i++) {
			// creating new HashMap
			HashMap<String, String> map = new HashMap<String, String>();
			Element e = (Element) nl.item(i);
			// adding each child node to HashMap key => value
			
			map.put(KEY_CD_CONTENT, parser.getValue(e, KEY_CD_CONTENT));
			map.put(KEY_CD_NAME, parser.getValue(e, KEY_CD_NAME));
			

			// adding HashList to ArrayList
			menuItems.add(map);
		}
        
        adapter = new SimpleAdapter(this, menuItems,
				R.layout.cd_display,
				new String[] { KEY_CD_NAME, KEY_CD_CONTENT}, new int[] {
						R.id.CDName, R.id.CDContents });

		setListAdapter(adapter); // populating the list with the data.
		      
    }
	
	// Method to search for the CDs.
	@SuppressWarnings("deprecation")
	public void onSearchClickCD(View view) {
          
		final String url = "http://10.100.55.81/Service1.asmx/SearchCD";
		String xml;
		txtSearchCD = (EditText) findViewById (R.id.txtSearchCD);
		btnSearchCD.setEnabled(false);
        menuItems = new ArrayList<HashMap<String, String>>();
		setListAdapter(adapter);
	
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		  
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost(url);

		    try {
		        // Add your data
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("keyword", txtSearchCD.getText().toString()));
		        
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		        // Execute HTTP Post Request
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity httpEntity = response.getEntity();
	            xml = EntityUtils.toString(httpEntity);
	            XMLParser parser = new XMLParser();
	            Document doc = parser.getDomElement(xml);
	            NodeList nl = doc.getElementsByTagName(KEY_ITEM);
	            
	            for (int i = 0; i < nl.getLength(); i++) {
	    			// creating new HashMap
	    			HashMap<String, String> map = new HashMap<String, String>();
	    			Element e = (Element) nl.item(i);
	    			// adding each child node to HashMap key => value
	    			
	    			map.put(KEY_CD_NAME, parser.getValue(e, KEY_CD_NAME));
	    			map.put(KEY_CD_CONTENT, parser.getValue(e, KEY_CD_CONTENT));
	    			

	    			// adding HashList to ArrayList
	    			menuItems.add(map);
	    		}
	            
	            adapter = new SimpleAdapter(this, menuItems,
	    				R.layout.cd_display,
	    				new String[] { KEY_CD_NAME, KEY_CD_CONTENT}, new int[] {
	    						R.id.CDName, R.id.CDContents });

	    		setListAdapter(adapter);
	            
		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    }
		    btnSearchCD.setEnabled(true);
		} 

}
