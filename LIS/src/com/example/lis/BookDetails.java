package com.example.lis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;


// to display the details of a particular book.
public class BookDetails extends ListActivity{
	
	static final String KEY_TITLE = "title";
 	static final String KEY_AVAILABLECOPIES = "availablecopies";
 	static final String KEY_ITEM = "IssuedBooks";
 	static final String KEY_COPY_ID = "Copy_Id";
 	static final String KEY_DUE_DATE = "Due_Date";
 	
 	ArrayList<HashMap<String, String>> menuItems;
 	ListAdapter adapter;
 	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_details);
        final String url = "http://10.100.55.81/Service1.asmx/DisplayBookDetails"; // url of the web service
        menuItems = new ArrayList<HashMap<String, String>>();
        
        Intent in = getIntent(); // Initializing the intent
        
        String title = in.getStringExtra(KEY_TITLE); // getting data from the previous screen.
        String availablecopies = in.getStringExtra(KEY_AVAILABLECOPIES);
        
        TextView lblTitle = (TextView) findViewById(R.id.lblTitle);// to display the title of the book.
        TextView lblAvailableCopies = (TextView) findViewById(R.id.lblAvailableCopies); // to display the number of available copies.
        
        lblTitle.setText(title);
        lblAvailableCopies.setText(availablecopies);
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
        
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);
		try {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("title", title));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity httpEntity = response.getEntity();
			String xml = EntityUtils.toString(httpEntity);
			 
			XMLParser parser = new XMLParser();
			Document doc = parser.getDomElement(xml);
			NodeList nl = doc.getElementsByTagName(KEY_ITEM);
			
			for (int i = 0; i < nl.getLength(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				Element e = (Element) nl.item(i);
				map.put(KEY_COPY_ID, parser.getValue(e, KEY_COPY_ID));
    			map.put(KEY_DUE_DATE, parser.getValue(e, KEY_DUE_DATE).substring(0, 10));
    			
    			menuItems.add(map);
			}
			adapter = new SimpleAdapter(this, menuItems,
    				R.layout.single_book_details,
    				new String[] { KEY_COPY_ID, KEY_DUE_DATE}, new int[] {
    						R.id.copy_id, R.id.due_date });
			setListAdapter(adapter);
			
		}
		 catch (Exception e) {
			// TODO: handle exception
		}
       
    }

}
