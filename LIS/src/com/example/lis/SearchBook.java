package com.example.lis;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SearchBook extends ListActivity {

	 protected EditText txtSearch;
	 
     static final String KEY_ITEM = "book"; // parent node
     static final String KEY_NAME = "title";
     static final String KEY_AVAILCOPIES = "availablecopies";
     static final String KEY_BOOKID = "book_id";
     static final String KEY_AUTHOR = "author";
     static final String KEY_SUBJECT = "subject";
     static final String KEY_EDITION = "edition";
     static final String KEY_PUBLISHER = "publisher";
     static final String KEY_TOTALCOPIES = "totalcopies";
     static final String KEY_ISSUEDCOPIES = "issuedcopies";
     ListAdapter adapter;
     private ArrayList<String> array_sort= new ArrayList<String>();
     protected Button btnSearch;
    
     ArrayList<HashMap<String, String>> menuItems;
     ArrayList<HashMap<String, String>> searchResults;
     
    
     	 
	public void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.main);
        
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setEnabled(true);
        
        final String url = "http://10.100.55.81/Service1.asmx/ListAllBooks";
        menuItems = new ArrayList<HashMap<String, String>>();
     
        if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
        
        XMLParser parser = new XMLParser();
        String xml = parser.getXmlFromUrl(url);// getting xml data
        Document doc = parser.getDomElement(xml);// getting the DOM element of the XML data.
        NodeList nl = doc.getElementsByTagName(KEY_ITEM);
        
        for (int i = 0; i < nl.getLength(); i++) {
			// creating new HashMap
			HashMap<String, String> map = new HashMap<String, String>();
			Element e = (Element) nl.item(i);
			// adding each child node to HashMap key => value
			
			map.put(KEY_NAME, parser.getValue(e, KEY_NAME));
			map.put(KEY_AVAILCOPIES, parser.getValue(e, KEY_AVAILCOPIES));
			

			// adding HashList to ArrayList
			menuItems.add(map);
		}
        
        adapter = new SimpleAdapter(this, menuItems,
				R.layout.search_book,
				new String[] { KEY_NAME, KEY_AVAILCOPIES}, new int[] {
						R.id.title, R.id.availablecopies });

		setListAdapter(adapter);
		
		ListView lvBooks = getListView();
		// Clicking on the list item to see the details of a particular book.
		lvBooks.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// getting values from selected ListItem
				String title = ((TextView) view.findViewById(R.id.title)).getText().toString();
				String availablecopies = ((TextView) view.findViewById(R.id.availablecopies)).getText().toString();
				
				
				// Starting new intent
				Intent in = new Intent(getApplicationContext(), BookDetails.class);
				in.putExtra(KEY_NAME, title);
				in.putExtra(KEY_AVAILCOPIES, availablecopies);
				
				startActivity(in);

			}
		});

	
	}
	
	@SuppressWarnings("deprecation")
	public void onSearchClick(View view) {
          
		final String url = "http://10.100.55.81/Service1.asmx/SearchBook";
		String xml;
		txtSearch = (EditText) findViewById (R.id.txtSearch);
       
        menuItems = new ArrayList<HashMap<String, String>>();
		setListAdapter(adapter);
	
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = 
				new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		
	 
		    HttpClient httpclient = new DefaultHttpClient();
		    HttpPost httppost = new HttpPost(url);

		    try {
		        // Add your data
		        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		        nameValuePairs.add(new BasicNameValuePair("keyword", txtSearch.getText().toString()));
		        
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		        // Execute HTTP Post Request
		        HttpResponse response = httpclient.execute(httppost);
		        HttpEntity httpEntity = response.getEntity();
	            xml = EntityUtils.toString(httpEntity);
	            XMLParser parser = new XMLParser();
	            Document doc = parser.getDomElement(xml);
	            NodeList nl = doc.getElementsByTagName(KEY_ITEM);
	            
	            for (int i = 0; i < nl.getLength(); i++) {
	    			// creating new HashMap
	    			HashMap<String, String> map = new HashMap<String, String>();
	    			Element e = (Element) nl.item(i);
	    			// adding each child node to HashMap key => value
	    			
	    			map.put(KEY_NAME, parser.getValue(e, KEY_NAME));
	    			map.put(KEY_AVAILCOPIES, parser.getValue(e, KEY_AVAILCOPIES));
	    			

	    			// adding HashList to ArrayList
	    			menuItems.add(map);
	    		}
	          
	            btnSearch.setEnabled(false);
	            
	            adapter = new SimpleAdapter(this, menuItems,
	    				R.layout.search_book,
	    				new String[] { KEY_NAME, KEY_AVAILCOPIES}, new int[] {
	    						R.id.title, R.id.availablecopies });

	    		setListAdapter(adapter);
	            
		    } catch (ClientProtocolException e) {
		        // TODO Auto-generated catch block
		    } catch (IOException e) {
		        // TODO Auto-generated catch block
		    }
		    btnSearch.setEnabled(true);
		} 
	
		
       
	
	
	
}
